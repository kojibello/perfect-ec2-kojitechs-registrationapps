output "dns_names" {
    depends_on = [
      aws_route53_record.dns_record
    ]
  value = format("https://%s", resource.aws_route53_record.dns_record.name)
}